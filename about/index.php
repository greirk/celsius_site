<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle("О компании");

?><div class="section about-firstscreen">
	<div class="section__full">
		<div class="about-firstscreen__content">
			<div>
				<div class="breadcrumbs">
					<div href="/" class="item">
 <a href="/">
						Главная </a>
					</div>
				</div>
				<h1 class="has-text-white">
				О компании </h1>
			</div>
			<div class="about-stats is-hidden-touch">
				<div>
					<div class="left-top-border">
						<div class="is-size-2 is-size-0-touch about-stats__title">
							40
						</div>
						<div>
							человек работает в&nbsp;компании
						</div>
					</div>
				</div>
				<div>
					<div class="left-top-border">
						<div class="is-size-2 is-size-0-touch about-stats__title">
							5000+
						</div>
						<div>
							построено объектов с нашим участием
						</div>
					</div>
				</div>
				<div>
					<div class="left-top-border">
						<div class="is-size-2 is-size-0-touch about-stats__title">
							80%
						</div>
						<div>
							клиентов обращаются к нам повторно
						</div>
					</div>
				</div>
				<div>
					<div class="left-top-border">
						<div class="is-size-2 is-size-0-touch about-stats__title">
							33
						</div>
						<div>
							города – география наших поставок
						</div>
					</div>
				</div>
				<div>
					<div class="left-top-border">
						<div class="is-size-2 is-size-0-touch about-stats__title">
							4
						</div>
						<div>
							страны закупают наше оборудование
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section is-hidden-desktop">
	<div class="section__full">
		<div class="about-stats">
			<div>
				<div class="left-top-border">
					<div class="is-size-2 is-size-0-touch about-stats__title">
						40
					</div>
					<div>
						человек работает в&nbsp;компании
					</div>
				</div>
			</div>
			<div>
				<div class="left-top-border">
					<div class="is-size-2 is-size-0-touch about-stats__title">
						5000
					</div>
					<div>
						построено объектов с нашим участием
					</div>
				</div>
			</div>
			<div>
				<div class="left-top-border">
					<div class="is-size-2 is-size-0-touch about-stats__title">
						80%
					</div>
					<div>
						клиентов обращаются к нам повторно
					</div>
				</div>
			</div>
			<div>
				<div class="left-top-border">
					<div class="is-size-2 is-size-0-touch about-stats__title">
						33
					</div>
					<div>
						города – география наших поставок
					</div>
				</div>
			</div>
			<div>
				<div class="left-top-border">
					<div class="is-size-2 is-size-0-touch about-stats__title">
						4
					</div>
					<div>
						страны закупают наше оборудование
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section with-divider">
	<div class="section__full">
		<div class="staff">
			<div class="staff__item">
 <img src="/static/img/photo1.jpg">
				<div class="is-size-3 has-text-black">
					Алексей Роговский
				</div>
				<div>
					Владелец компании
				</div>
			</div>
			<div class="staff__item">
				<img src="/static/img/photo3.jpg">
				<div class="is-size-3 has-text-black">
					Иван Устименко
				</div>
				<div>
					Владелец компании
				</div>
			</div>
			<div class="staff__item">
				<img src="/static/img/photo2.jpg">
				<div class="is-size-3 has-text-black">
					Александр Забава
				</div>
				<div>
					Исполнительный директор
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section with-divider">
	<div class="section__full">
		<h2 class="has-text-black">Хотите стать частью команды?</h2>
		<p class="is-size-3 pt-6">
			 Возможно, мы ищем именно вас, смотрите <a href="/vacancies" class="base-link link icon-null"> <span class="base-link__text">вакансии</span> </a>
			.
		</p>
	</div>
</div>
<?
$APPLICATION->IncludeFile(
    SITE_DIR . "include/feedback_form.php",
    array(
        "title" => "Обращайтесь!",
        "text" => "Получите экспертизу, подтвержденную 10-летним опытом и, как результат, наиболее выгодное и технически верное решение",
        "name" => "Форма обратной связи",
        "contacts_content" => '
                <h3 class="has-text-black">
                    Уже есть готовая смета?
                </h3>
                <p class="pt-4">
                    Отправьте её&nbsp;нам
                    <a href="mailto:sales@celsiy.pro" class="base-link pt-4 pt-2-touch link icon-null">
                        <span class="base-link__text">sales@celsiy.pro</span>
                    </a>
                    или свяжитесь с&nbsp;нами по&nbsp;телефону
                    <a href="tel:+7 (3952) 920-905" class="has-text-black">+7 (3952) 920-905</a>
                    и&nbsp;мы&nbsp;бесплатно улучшим её&nbsp;без потерь в&nbsp;качестве и&nbsp;с&nbsp;большей выгодой для&nbsp;вас!
                </p>
            '
    ),
    array("MODE" => "html")
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>