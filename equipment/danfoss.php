<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Цельсий/Danfoss");
$APPLICATION->SetPageProperty("keywords", "Danfoss");
$APPLICATION->SetTitle("Danfoss");
?>

<p>Danfoss (Данфосс) — один из крупнейших промышленных концернов в Дании.</p>
<p>Группа Данфосс является глобальным лидером в разработке, производстве, продажах и обслуживании механических и электронных компонентов для промышленности. Бизнес концерна Данфосс сосредоточен на трех главных направлениях, где Данфосс занимает лидирующие позиции на рынке:</p>
<ul>
	<li>тепловая автоматика;</li>
	<li>общепромышленная и запорная арматура;</li>
	<li>приводная техника.</li>
</ul>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>