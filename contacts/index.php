<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("contacts");
?>    <!--    <div id="widget-contacts"></div>-->
    <div id="widget-contacts">
        <div data-title="Офис"
             data-coords="[104.286302, 52.284514]"
             data-name="office"
             data-icon="red_marker"
             data-address="Иркутск, ул. Горького 36Б, БЦ «Максим», 7 этаж, офис 1-17"
             data-schedule="Понедельник – Пятница с 09:00 – 17:30"
             data-phone="+7 (3952) 920-905"
             data-email="office@celsiy.pro"
             data-color="accent"
        ></div>
        <div data-title="Склад"
             data-coords="[104.263431, 52.342901]"
             data-name="warehouse"
             data-icon="blue_marker"
             data-address="Иркутск, ул. Полярная, 117а"
             data-schedule="Понедельник – Пятница с 09:00 – 17:30"
             data-phone="+7 (3952) 920-905"
             data-email=""
             data-color="surface"
        ></div>
    </div>
    <div class="section with-divider">
        <div class="section__half">
            <p class="has-text-weight-bold has-text-black">Реквизиты</p>
            <p>Общество с ограниченной ответственностью «Цельсий»</p>
            <p>ИНН/КПП: 3849013904/380801001</p>
            <p>ОГРН: 1113850002914</p>
            <p>Юр. адрес: 664 011, Россия, г. Иркутск, ул. Горького, 36Б, офис 1-17.</p>
        </div>
        <div class="section__half-second">
            <p class="has-text-weight-bold has-text-black">Банковские реквизиты</p>
            <p>СИБИРСКИЙ ФИЛИАЛ АО «РАЙФФАЙЗЕНБАНК» Г. НОВОСИБИРСК</p>
            <p>БИК: 045004799</p>
            <p>Р/с: 40702810407000012432</p>
            <p>К/с: 30101810300000000799</p>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>