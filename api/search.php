<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("search");

$obSearch = new CSearch;
$obSearch->Search(array(
    'QUERY' => $_REQUEST['text'],
    'SITE_ID' => LANG,
    'MODULE_ID' => 'iblock',
));
$obSearch->NavStart();

$out = Array();
while ($arSearch = $obSearch->Fetch()) {
//    print_r($arSearch);
    $result = CIBlockElement::GetByID($arSearch["ITEM_ID"]);

    $result->SetUrlTemplates("/#IBLOCK_CODE#/#ELEMENT_CODE#/");
    $item = $result->GetNextElement()->fields;
//    print_r($item->fields["DETAIL_PAGE_URL"]);
    if (isset($item["NAME"]) && isset($item["DETAIL_PAGE_URL"])) {
        $out[] = array(
            "NAME" => $item["NAME"],
            "DETAIL_PAGE_URL" => ($item["IBLOCK_CODE"] !== "vendors" ? $item["DETAIL_PAGE_URL"] : "/".$item["IBLOCK_TYPE_ID"].$item["DETAIL_PAGE_URL"]),
            "BLOCK_CODE" => $item
        );
    }
}
echo json_encode($out);
?>

<?php
///**
// * Created by PhpStorm.
// * User:
// * Date: 14.09.2018
// * Time: 12:51
// */
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//CModule::IncludeModule("iblock");
////http://ck21318.tmweb.ru/include/ajax/search.php?text=краб
//
//\Bitrix\Main\Loader::includeModule('iblock');
//$text=$_REQUEST['text'];
//$dbItems = \Bitrix\Iblock\ElementTable::getList(array(
//    'select' => array('ID', 'NAME', 'IBLOCK_ID' , 'PREVIEW_PICTURE', ), //,'DETAIL_PAGE_URL'
//    'filter' => array('NAME'=>'%'.strip_tags($text).'%'),
//    'limit'=>10
//));
//
//$dbItems->SetUrlTemplates("/#SECTION_CODE#/#ELEMENT_CODE#");
//
//$arrResult=array();
//
//while ($arItem = $dbItems->fetch()){
//    //DETAIL_PAGE_URL d7 не выводит!!!
//
//    $el_res= CIBlockElement::GetByID( $arItem['ID'] );
//    if ( $el_arr= $el_res->GetNext() ) {
//        $arItem['DETAIL_PAGE_URL']= $el_arr[ 'DETAIL_PAGE_URL' ];
//        $arItem['PREVIEW_PICTURE']=  CFile::GetPath( $el_arr[ 'PREVIEW_PICTURE' ]  ) ;
//        }
//   // debug($arItem);
//    $arrResult[]=$arItem;
//}
//echo json_encode($arrResult);
//?>