<?php
$arJsConfig = array(
    'custom_main' => array(
        'js' => 'https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js',
    )
);

foreach ($arJsConfig as $ext => $arExt) {
    \CJSCore::RegisterExt($ext, $arExt);
}