<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/include/recaptcha.php");

$nuxt_static_path="/local/js/celsius/frontend/dist";
$nuxt_widgets_path="/local/js/celsius/frontend/dist_widgets";

$GLOBALS['site_phone'] = "+7 (3952) 920-905";
$GLOBALS['info_email'] = "info@celsiy.pro";

if ($USER->IsAdmin()) {
//   Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js');
} else {
//   Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js');
}
\Bitrix\Main\UI\Extension::load("ui.vue");
CJSCore::Init(array('popup'));
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<!DOCTYPE html>
<html>
<head>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta data-n-head="ssr" charset="utf-8">
    <meta data-n-head="ssr" name="viewport" content="width=device-width,initial-scale=1">
    <meta data-n-head="ssr" data-hid="description" name="description" content="">
    <meta data-n-head="ssr" name="msapplication-TileColor" content="#2b5797">
    <meta data-n-head="ssr" name="theme-color" content="#ffffff">
    <link data-n-head="ssr" rel="icon" type="image/x-icon" href="<?= $nuxt_static_path ?>/meta/favicon.ico">
    <link data-n-head="ssr" rel="apple-touch-icon" href="<?= $nuxt_static_path ?>/meta/apple-touch-icon.png">
    <link data-n-head="ssr" rel="icon" type="image/png" sizes="32x32" href="<?= $nuxt_static_path ?>/meta/favicon-32x32.png">
    <link data-n-head="ssr" rel="icon" type="image/png" sizes="16x16" href="<?= $nuxt_static_path ?>/meta/favicon-16x16.png">
    <link data-n-head="ssr" rel="manifest" href="<?= $nuxt_static_path ?>/meta/site.webmanifest">
    <link data-n-head="ssr" rel="mask-icon" href="<?= $nuxt_static_path ?>/meta/safari-pinned-tab.svg">
    <script data-n-head="ssr" src="https://yastatic.net/share2/share.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=<?= $recaptcha_site_key ?>"></script>
    <link rel="preload" href="<?= $nuxt_static_path ?>/_nuxt/vendors/app.css" as="style">
    <link rel="preload" href="<?= $nuxt_static_path ?>/_nuxt/app.css" as="style">
    <link rel="stylesheet" href="<?= $nuxt_static_path ?>/_nuxt/vendors/app.css">
    <link rel="stylesheet" href="<?= $nuxt_static_path ?>/_nuxt/app.css">
    <script>
        window.recaptcha_site_key = "<?= $recaptcha_site_key ?>"
    </script>
</head>
<body>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>