<?
require($_SERVER["DOCUMENT_ROOT"]."/local/templates/celsius/head.php");
$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$is_main = $path_only === '/';
?>
<div class="main-container landing" >
    <div class="left-border"></div>
    <div class="content-container">
        <? if ($is_main): ?>
        <div class="main-header" >
            <div class="cont columns is-gapless" >
                <div class="column is-7" >
                    <div class="left-header"  >
                        <div class="column is-half is-half-touch" ><a href="/" ><img
                                        src="/img/logo.svg" class="logo" ></a></div>
                        <div class="column is-half has-text-right is-hidden-touch" >
                            <div class="header-phone is-size-4 has-text-light" >
                                <a href="tel:<?=$GLOBALS['site_phone']?>" class="has-text-light">
                                    <?=$GLOBALS['site_phone']?>
                                </a>
                            </div>
                            <a href="#feedback" class="base-link link-main icon-arrowRight" >
                                <div class="base-link__text">
                                    Заказать звонок
                                </div> <!----></a></div>
                        <div class="column is-hidden-desktop is-half-touch has-text-right">
                            <div id="widget-hamburger"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? else: ?>
            <div class="header" >
                <div class="header__logo-column" >
                    <div class="left-header"  >
                        <div class="column is-half is-half-touch" ><a href="/" ><img
                                        src="/img/logo.svg" class="logo" ></a></div>
                        <div class="column is-half has-text-right is-hidden-touch" >
                            <div class="header-phone is-size-4 has-text-light" >
                                <a href="tel:<?=$GLOBALS['site_phone']?>" class="has-text-light">
                                    <?=$GLOBALS['site_phone']?>
                                </a>
                            </div>
                            <a href="#feedback" class="base-link link-main icon-arrowRight" >
                                <span class="base-link__text">Заказать звонок</span>
                            </a>
                        </div>
                        <div class="column is-hidden-desktop is-half-touch has-text-right" >
                            <div id="widget-hamburger"></div>
                        </div>
                    </div>
                </div>
                <div class="header__menu-column" >
                    <div id="widget-header-menu"></div>
                </div>
            </div>
        <? endif; ?>

            <?php
//                $APPLICATION->IncludeFile("breadcrumbs.php")
            ?>