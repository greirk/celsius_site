<div>
    <div>
        <div class="is-size-3 is-size-2-touch phone-text">
            <a href="tel:<?= $GLOBALS['site_phone'] ?>" class="has-text-white">
                <?= $GLOBALS['site_phone'] ?>
            </a>
        </div>
        <div>
            <a href="#feedback" class="base-link link-light icon-arrowRight"><span class="base-link__text">
                                Заказать звонок
                              </span> <!----></a>
        </div>
    </div>
    <div class="pt-4 pt-4-touch">
        <div>
            Эл. почта
        </div>
        <div>
            <a href="mailto:sales@celsiy.pro" class="base-link link-light icon-arrowRight"><span
                    class="base-link__text">
                                sales@celsiy.pro
                              </span> <!----></a>
        </div>
    </div>
    <div class="pt-4 pt-4-touch">
        <div>
            Офис
        </div>
        <div class="has-text-link-light">
            Иркутск, ул. Горького 36Б, БЦ «Максим»,<br>7 этаж, офис 1-17
        </div>
    </div>
    <div class="pt-4 pt-4-touch">
        <div>
            Склад
        </div>
        <div class="has-text-link-light">
            Иркутск, ул. Полярная, 117а
        </div>
    </div>
    <div class="pt-4 pt-4-touch">
        <div>
            Мы в социальных сетях
        </div>
        <div class="footer-media-links has-text-link-light">
            <a href="https://www.instagram.com/celsiy38/" class="has-text-link-light">
                <img src="/img/icons/instagram_footer.png">
            </a>
        </div>
    </div>
</div>
<div>
    <div class="pt-4-touch">
        <div id="widget-requisites">
            <div class="footer-requisite">
                <p>Общество с&nbsp;ограниченной ответственностью «Цельсий»</p>
                <p>ИНН/КПП: 3849013904/380801001</p>
                <p>ОГРН: 1113850002914</p>
                <p>Юр. адрес: 664&nbsp;011, Россия, г.&nbsp;Иркутск, ул. Горького, 36Б, офис 1-17.</p>
                <p class="has-text-accent-light pt-6">Банковские реквизиты</p>
                <p>СИБИРСКИЙ ФИЛИАЛ АО&nbsp;«РАЙФФАЙЗЕНБАНК» Г. НОВОСИБИРСК</p>
                <p>БИК: 045004799</p>
                <p>Р/с: 40702810407000012432</p>
                <p>К/с: 30101810300000000799</p>
            </div>
        </div>
    </div>
</div>
<div class="footer-right__copyright is-hidden-desktop">
    <p class="has-text-link-light">
        © 2011–2021, ООО Цельсий, Иркутск
    </p>
    <a class="base-link link-light icon-null">
        <span class="base-link__text">Политика конфиденциальности</span>
    </a>
</div>