<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<div class="footer has-text-white">
    <div class="footer-left column-3-4 is-hidden-touch">
        <div>
            <? $APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", array(
                    "ROOT_MENU_TYPE" => "footer",
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            ); ?>
        </div>
        <div class="is-justify-content-flex-end"><span class="has-text-link-light mr-3">
                            © 2011–2021, ООО Цельсий, Иркутск
                          </span>
            <a class="base-link link-light icon-null" href="/privacy"><span class="base-link__text">
                            Политика конфиденциальности
                          </span> <!----></a>
        </div>
    </div>
    <div class="footer-right column-1-4">
        <?
        $APPLICATION->IncludeFile(
            SITE_DIR . "local/templates/celsius/footer_right.php",
            array(),
            array("MODE" => "html")
        );
        ?>
    </div>
</div>
</div>
<div class="right-border"></div>
</div>
<script src="<?= $nuxt_static_path ?>/_nuxt/vendors/app.js" defer></script>
<script src="<?= $nuxt_static_path ?>/_nuxt/app.js" defer></script>
<script src="<?= $nuxt_widgets_path ?>/widgets.umd.js" defer></script>
</body>
</html>