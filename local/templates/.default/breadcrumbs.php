<?php
$hide_breadcrumbs_on_pages = ["/", "/equipment/", "/contacts/"];
$hide_breadcrumbs = array_search($APPLICATION->GetCurPage(false), $hide_breadcrumbs_on_pages);
?>

<? if (!$hide_breadcrumbs): ?>
    <div class="section pb-0">
        <div class="section__full pb-0">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "chain", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
        </div>
    </div>
<? endif; ?>
