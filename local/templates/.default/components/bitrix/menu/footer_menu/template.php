<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="footer-menu has-text-white is-size-2">
        <?foreach($arResult as $arItem):?>
            <?if($arItem["SELECTED"]):?>
                <div><a href="<?=$arItem["LINK"]?>" class="active">
                        <?=$arItem["TEXT"]?></a></div>
            <?else:?>
                <div><a href="<?=$arItem["LINK"]?>">
                        <?=$arItem["TEXT"]?></a></div>
            <?endif?>
        <?endforeach?>
    </div>
<?endif?>