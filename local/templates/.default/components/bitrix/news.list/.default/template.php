<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


$component_pagenav = $this->getComponent();

//function category_filter($arItem)
//{
//    return $arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"] == "news";
//}
//
//$items = array_filter($arResult["ITEMS"], "category_filter");
$items = $arResult["ITEMS"];

$this->setFrameMode(true);
?>
<div class="news-list pb-6">
    <? foreach ($items as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="article-preview">
            <div class="article-preview__category">
                <span><?= $arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["DISPLAY_VALUE"] ?></span>
            </div>
            <div class="article-preview__content">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                   class="base-link is-size-2 is-size-2-touch simple link icon-null">
                    <span class="base-link__text">
                        <? echo $arItem["NAME"] ?>
                    </span>
                </a>
                <div class="article-preview__text pt-3">
                    <? echo $arItem["PREVIEW_TEXT"]; ?>
                </div>
            </div>
            <div class="article-preview__img">
                <img
                        class="preview_picture"
                        border="0"
                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                        width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                        height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                        style="float:left"
                />
            </div>
        </div>
    <? endforeach; ?>
</div>

<?php
echo $arResult["NAV_RESULT"]->GetPageNavStringEx(
    $navComponentObject,
    "",
    "article_pagenavigation"
);
?>

<div class="section with-divider px-0">
    <div class="section__full px-0">
        <div id="widget-subscribe" name="Форма подписки"></div>
    </div>
</div>

<?
$APPLICATION->IncludeFile(
    SITE_DIR . "include/feedback_form.php",
//    array(
//        "title" => "Обращайтесь!",
//        "text" => "Получите экспертизу, подтвержденную 9-летним опытом и, как результат, наиболее выгодное и технически верное решение",
//        "name" => "Форма обратной связи",
//        "zero_x_paddings" => true
//    ),
    array("MODE" => "html")
);
?>
