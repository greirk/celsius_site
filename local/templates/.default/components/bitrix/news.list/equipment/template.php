<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$j = file_get_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendors.json");
// $vendors = json_decode($j);
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "CODE");
$arFilter = Array("IBLOCK_ID"=>IntVal(9), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
$vendors = Array();
while($ob = $res->GetNextElement())
{
    $vendors[] = $ob->GetFields();
}

?>
    <div class="section with-divider">
        <div class="section__half with-divider">
            <div class="breadcrumbs">
                <div href="/" class="item"><a href="/">
                        Главная
                    </a></div>
                <div href="/equipment" class="item">
                    <a href="/equipment">
                        Оборудование
                    </a>
                </div>
            </div>
            <!--            --><? //$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain",Array(
            //                    "START_FROM" => "0",
            //                    "PATH" => "",
            //                    "SITE_ID" => "s1"
            //                )
            //            );?>

            <h1>Оборудование</h1>
            <div class="pt-14">
                <div class="icons-menu is-size-3 is-size-3-touch">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="caret">
                            <span class="is-size-3 is-size-3-touch">
                                 <? echo $arItem["NAME"] ?>
                            </span>
                        </a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <div class="section__half pipes3-bg">
            <div class="is-size-2 is-size-2-touch has-text-black">
                Производители оборудования
            </div>
            <div class="pt-104">
                <div class="columns is-gapless">
                    <div class="column">
                        <div class="icons-menu mini">
                            <? for ($i = 0; $i < 9; $i++): $vendor = $vendors[$i]; ?>
                                <a href="/equipment/vendors/<?= $vendor['CODE'] ?>/" class="caret">
                                    <span>
                                      <?= $vendor['NAME'] ?>
                                    </span>
                                </a>
                            <? endfor; ?>
                        </div>
                    </div>
                    <div class="column">
                        <div class="icons-menu mini">
                            <? for ($i = 9; $i < sizeof($vendors); $i++): $vendor = $vendors[$i]; ?>
                                <a href="/equipment/vendors/<?= $vendor['CODE'] ?>/" class="caret">
                                    <span>
                                      <?= $vendor['NAME'] ?>
                                    </span>
                                </a>
                            <? endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
