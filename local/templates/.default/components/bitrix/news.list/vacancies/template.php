<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//$APPLICATION->IncludeFile("breadcrumbs.php");
?>
    <script>
        const toggleVacancy = function (id) {
            const wrapper = document.querySelector(`#vacancy-${id}`);
            const link = wrapper.querySelector(`#${id}`)
            const content = wrapper.querySelector(`.collapse-block__content`);

            const showed = content.classList.contains('active');

            if (showed) {
                content.classList.remove('active');
                link.classList.remove('icon-arrowDown')
                link.classList.add('icon-arrowRight')
            } else {
                content.classList.add('active');
                link.classList.add('icon-arrowDown')
                link.classList.remove('icon-arrowRight')
            }
        }
    </script>
    <div class="section with-divider">
        <div class="section__full">
            <div class="breadcrumbs">
                <div href="/" class="item">
                    <a href="/">
                        Главная
                    </a>
                </div>
            </div>
            <h1 class="has-text-black">
                Вакансии
            </h1>
            <div class="section__columns pt-14">
                <div class="column-3-4 offset-40">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <div class="collapse-block with-divider" id="vacancy-<?=$arItem["CODE"] ?>">
                        <a href="#<?=$arItem["CODE"] ?>" id="<?=$arItem["CODE"] ?>"
                           class="base-link collapse-block__title is-size-2 is-size-2-touch link icon-arrowRight"
                           onclick="toggleVacancy('<?=$arItem["CODE"] ?>')"
                        >
                            <span class="base-link__text">
                                <?=$arItem["NAME"] ?>
                            </span>
                        </a>
                        <div class="collapse-block__content">
                            <?=$arItem["PREVIEW_TEXT"] ?>
                            <a href="mailto:hr@celsiy.pro" class="base-link link icon-arrowRight">
                                <span class="base-link__text">
                                  Отправить резюме
                                </span>
                            </a>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
                <div class="column-1-4">
                    <div class="left-top-border simple mobile-simple">
                        <div class="pb-3 has-text-weight-bold">
                            Откликнуться на вакансию
                        </div>
                        <p class="pt-3">
                            Резюме направляйте на адрес
                            <a href="mailto:hr@celsiy.pro" class="base-link link icon-null"><span
                                        class="base-link__text">hr@celsiy.pro</span> <!----></a>
                            или через сайт
                            <a href="hh.ru" class="base-link link icon-null"><span class="base-link__text">hh.ru</span>
                                <!----></a>
                            . Собеседование проходит в три этапа:
                        </p>
                        <ol>
                            <li>
                                Предварительная беседа по скайпу (видео-интервью);
                            </li>
                            <li>
                                Очная встреча;
                            </li>
                            <li>
                                Финальная встреча с собственником.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
