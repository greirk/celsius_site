<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//$APPLICATION->IncludeFile("breadcrumbs.php");
?>
<div class="section">
    <div class="section__full">
        <div class="breadcrumbs">
            <div href="/" class="item">
                <a href="/">
                    Главная
                </a>
            </div>
        </div>
        <h1>Кейсы объектов</h1>
    </div>
</div>
<div class="section with-divider">
    <div class="columns is-gapless is-flex-wrap-wrap">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="column is-half is-full-touch">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="case-card " style="background-image: url(/img/cases/bg-gradient.png), url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>) ">
                    <h3 class="case-card__title has-text-white">
                        <? echo $arItem["NAME"] ?>
                    </h3>
                    <div class="case-card__description">
                        <?= $arItem["PREVIEW_TEXT"] ?>
                    </div>
                    <span class="base-link case-card__link is-size-3 link-white icon-caret">
                            <span class="base-link__text">Смотреть кейс</span>
                        </span>
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>
