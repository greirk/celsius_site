<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="article-pager">
    <?if ($arResult["NavPageNomer"] > 1) {?>
        <?if ($arResult["NavPageNomer"] > 2) {?>
            <a class="article-pager__left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
        <?} else {?>
            <a class="article-pager__left" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"></a>
        <?}?>
    <?} else { // Если страница первая?>
        <a class="article-pager__left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
    <?}?>
    <?$page = $arResult["nStartPage"]?>
    <?while($page <= $arResult["nEndPage"]) {?>
        <div class="article-pager__counter">
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page?>"
               class="base-link article-pager__page link icon-null <?= ($page == $arResult["NavPageNomer"] ? 'active' : '') ?>">
                <span class="base-link__text"><?=$page?></span>
            </a>
        </div>
        <?$page++?>
    <?}?>
    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="article-pager__right"></a>
</div>