<?php
/*
 * Файл /local/templates/blog/components/bitrix/news/blog/bitrix/news.detail/.default/template.php
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>

<article id="blog-article">
    <div class="section with-divider">
        <div class="section__full">
            <div class="article-wrapper">
                <div class="article-wrapper__left">
                    <div class="article-wrapper__category">
                        <?= $arResult['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE'] ?>
                    </div>
                    <div class="article-wrapper__shares is-hidden-touch">
                        <div data-curtain data-size="l" data-shape="round"
                             data-services="vkontakte,facebook,odnoklassniki,twitter" class="ya-share2"
                        ></div>
                    </div>
                </div>
                <div class="article-wrapper__container">
                    <div>
                        <h1 class="has-text-black">
                            <?= $arResult['NAME'] ?>
                        </h1>
                        <?= $arResult['DETAIL_TEXT']; ?>
                    </div>
                </div>
            </div>
        </div>
        <?
        
        $APPLICATION->IncludeFile(
            SITE_DIR . "include/feedback_form.php",
            array(
                "title" => $arResult["PROPERTIES"]["form_title"]["VALUE"],
                "text" => $arResult["PROPERTIES"]["form_text"]["VALUE"],
                "name" => "Форма обратной связи",
                "has_smeta_block" => false
            ),
            array("MODE" => "html")
        );
        ?>
    </div>
</article>