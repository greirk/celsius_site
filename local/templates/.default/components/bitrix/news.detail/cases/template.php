<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>

<article id="blog-cases">
    <div class="columns is-gapless is-flex-wrap-wrap">
        <div class="case-begin">
            <div class="case-begin__title">
                <div class="breadcrumbs">
                    <div href="/" class="item">
                        <a href="/">
                            Главная
                        </a>
                    </div>
                    <div href="/cases" class="item">
                        <a href="/cases">
                            Кейсы
                        </a>
                    </div>
                </div>
                <h2 class="has-text-black is-hidden-touch">
                    <?= $arResult['NAME'] ?>
                </h2>
                <h1 class="has-text-black is-hidden-desktop">
                    <?= $arResult['NAME'] ?>
                </h1>
            </div>
            <div class="case-begin__writer "
                 style="background-image: url('<?= $arResult['DISPLAY_PROPERTIES']['author_photo']["FILE_VALUE"]["SRC"] ?>')"
            >
                <div class="quotes-block"></div>
                <div class="writer">
                    <div class="writer__title">Рассказывает</div>
                    <div class="writer__who">

                        <div class="is-capitalized">
                            <?= $arResult['DISPLAY_PROPERTIES']['author_name']['VALUE'] ?>
                        </div>
                        <div><?= $arResult['DISPLAY_PROPERTIES']['author_position']['VALUE'] ?></div>
                    </div>
                </div>
            </div>
            <div class="case-begin__img"
                 style="background-image: url('<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>')"
            ></div>
            <div class="case-begin__table_of_contents">
                <div class="has-text-weight-bold has-text-black">Оглавление</div>
                <div class="icons-menu mini"><a href="#problem" class="arrow-down"><span
                        >
                      Задача
                    </span></a><a href="#solution" class="arrow-down"><span>
                      Решение
                    </span></a><a href="#project" class="arrow-down"><span>
                      Проект
                    </span></a><a href="#result" class="arrow-down"><span>
                      Результат
                    </span></a></div>
            </div>
        </div>
    </div>
    <?= $arResult['DETAIL_TEXT']; ?>
    <div>
        <!--        --><? // var_dump($arResult['NEXT']) ?>
        <? if (is_array($arResult["TORIGHT"])): ?>
            <a href="<?= $arResult["TORIGHT"]["URL"] ?>" class="read-more-section section section--accent">
                <div class="section__full">
                    <h2 class="slight">
                        <span>Следующий кейс</span>
                    </h2>
                    <span class="is-size-2 is-size-2-touch read-more-section__title">
                    <?= $arResult["TORIGHT"]["NAME"] ?>
                </span>
                    <span class="base-link read-more-section__link link-white icon-caret">
                    <span class="base-link__text">
                        <span>Смотреть кейс</span>
                    </span>
                </span>
                </div>
            </a>
        <? endif ?>
    </div>
</article>