<?php
/*
 * Файл /local/templates/blog/components/bitrix/news/blog/bitrix/news.detail/.default/template.php
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
//$APPLICATION->IncludeFile("breadcrumbs.php");
?>
<div class="section pb-0">
    <div class="section__full pb-0">
        <div class="breadcrumbs">
            <div href="/" class="item">
                <a href="/">
                    Главная
                </a>
            </div>
            <div class="item">
                <a href="/equipment">
                    Оборудование
                </a>
            </div>
        </div>
    </div>
</div>

<article id="blog-equipment">
    <?= $arResult['DETAIL_TEXT']; ?>
</article>