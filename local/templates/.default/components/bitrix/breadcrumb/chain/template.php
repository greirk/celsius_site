<?php
/*
 * Файл local/templates/.default/components/bitrix/breadcrumb/chain/template.php
 */

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(empty($arResult)) {
    return '';
}


$items = array();
try {
    foreach ($arResult as $item) {
        if (!empty($item['LINK']) && $APPLICATION->GetCurPage(false) != $item['LINK'] && (!isset($prevItem) || $prevItem['LINK'] != $item['LINK'])) {
            $items[] = '<div class="item"><a href="' . $item['LINK'] . '">' . htmlspecialchars($item['TITLE']) . '</a></div>';
        } else {

        }
        $prevItem = $item;
    }
} catch (Exception $e) {

}
$result = '<div class="breadcrumbs">' . implode('', $items) . '</div>';

return $result;