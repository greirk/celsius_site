<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

return [
	'css' => 'dist/frontend.bundle.css',
	'js' => 'dist/frontend.bundle.js',
	'rel' => [
		'main.core',
	],
	'skip_core' => false,
];