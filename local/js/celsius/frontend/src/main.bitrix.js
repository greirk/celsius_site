import Vue from 'vue/dist/vue.esm';
import VueMq from 'vue-mq'

import FirstScreenRight from "./widgets/FirstScreenRight";
import Hamburger from "./widgets/Hamburger";
import DistributorsInlineList from "./widgets/DistributorsInlineList";
import Screen5 from "./widgets/Screen5";
// import FeedbackSection from "./layouts/FeedbackSection";
import RequisitesPopup from "./widgets/RequisitesPopup";
import HeaderMenuWrapper from "./widgets/HeaderMenuWrapper";
import ContactsSection from "./widgets/ContactsSection";
import SubscribeBlock from "./widgets/SubscribeBlock";
import FeedbackForm from "./widgets/FeedbackForm";
import PhotoSlider from "./widgets/PhotoSlider";

// import Menu from "./layouts/header/Menu";

// Vue.component('Menu', Menu);

const base_url = "/";

Vue.use(VueMq, {
  // Default breakpoint for SSR
  defaultBreakpoint: 'sm',
  breakpoints: {
    sm: 768,
    md: 1024,
    lg: Infinity
  }
})

new Vue({
  el: '#widget-main_catalog_card',
  // render: h => h(FirstScreenRight),
  render(h) {
    return h(FirstScreenRight, {
      props: {
        base_url: base_url
      }
    })
  }
});

new Vue({
  el: '#widget-hamburger',
  render: h => h(Hamburger),
});

new Vue({
  el: '#widget-distributors-block',
  // render: h => h(DistributorsInlineList),
  render(h) {
    if (!this.$el.attributes.length) return false;
    return h(DistributorsInlineList, {
      props: {
        beforeText: this.$el.attributes["data-before-text"].value,
        afterText: this.$el.attributes["data-after-text"].value,
        distributors: Object.values(this.$el.children).map(item => (
          {
            title: item.attributes["data-title"].value,
            description: item.attributes["data-description"].value,
            name: item.attributes["data-name"].value,
            img: item.attributes.src.value
          }
        ))
      }
    })
  }
});

new Vue({
  el: '#widget-landing-screen-price',
  render(h) {
    if (!this.$el.innerHTML) return false;
    return h(Screen5, {
      props: {
        option1Title: this.$el.querySelector("#option-1-title").innerHTML,
        option1Content: this.$el.querySelector("#option-1-content").innerHTML,
        option2Title: this.$el.querySelector("#option-2-title").innerHTML,
        option2Content: this.$el.querySelector("#option-2-content").innerHTML,
      }
    })
  }
});

// new Vue({
//   el: '#widget-feedback',
//   render: h => h(FeedbackSection),
// });

// new Vue({
//   el: '#widget-requisites',
//   render: h => h(RequisitesPopup)
// });

new Vue({
  el: '#widget-requisites',
  render(h) {
    if (!this.$el.innerHTML) return false;
    return h(RequisitesPopup, {
      props: {
        content: this.$el.innerHTML
      }
    })
  }
});

new Vue({
  el: '#widget-header-menu',
  render: h => h(HeaderMenuWrapper),
});

// new Vue({
//   el: '#widget-contacts',
//   render: h => h(ContactsSection),
// });
new Vue({
  el: '#widget-contacts',
  render(h) {
    if (!this.$el.children.length) return false;
    return h(ContactsSection, {
      props: {
        markers: Object.values(this.$el.children).map(item => (
          {
            title: item.dataset.title,
            coords: item.dataset.coords,
            name: item.dataset.name,
            iconImg: item.dataset.icon,
            address: item.dataset.address,
            schedule: item.dataset.schedule,
            phone: item.dataset.phone,
            email: item.dataset.email,
            color: item.dataset.color
          }
        ))
      }
    })
  }
});

new Vue({
  el: '#widget-subscribe',
  render: h => h(SubscribeBlock),
});

new Vue({
  el: '#widget-feedback-form',
  render(h) {
    if (!this.$el.attributes.length) return false;

    return h(FeedbackForm, {
      props: {
        skin: (this.$el.attributes.skin ? this.$el.attributes.skin.value : undefined),
        form_name: (this.$el.attributes.name ? this.$el.attributes.name.value : undefined),
        base_url
      }
    })
  }
});

new Vue({
  el: '.widget-feedback-form',
  render(h) {
    if (!this.$el.attributes.length) return false;

    return h(FeedbackForm, {
      props: {
        skin: (this.$el.attributes.skin ? this.$el.attributes.skin.value : undefined),
        form_name: (this.$el.attributes.name ? this.$el.attributes.name.value : undefined),
        base_url
      }
    })
  }
});

new Vue({
  el: '.widget-photo-slider',
  render(h) {
    if (!this.$el.children.length) return false;
    return h(PhotoSlider, {
      props: {
        items: Object.values(this.$el.children).map(item => ({img: item.attributes.src.value}))
      }
    })
  }
});
