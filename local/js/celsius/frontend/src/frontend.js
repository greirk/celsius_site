import {Type} from 'main.core';

export class Frontend
{
	constructor(options = {name: 'Frontend'})
	{
		this.name = options.name;
	}

	setName(name)
	{
		if (Type.isString(name))
		{
			this.name = name;
		}
	}

	getName()
	{
		return this.name;
	}
}