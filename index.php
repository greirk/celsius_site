<?
//require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/vendor/autoload.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
//Dbogdanoff\Bitrix\Vue::includeComponent(['main_catalog_card']);
$APPLICATION->SetTitle("Главная");
?><div data-v-6f4bf982>
    <div class="first-screen cont columns is-gapless is-align-items-flex-end" data-v-04451c94 data-v-6f4bf982>
        <div class="column is-7" data-v-04451c94>
            <div class="first-screen-pipes is-hidden-touch" data-v-04451c94></div>
            <div class="cont-touch" data-v-04451c94>
                <h2 class="has-text-black first-screen-slogan" data-v-04451c94><span class="is-hidden-touch" data-v-04451c94>
                        С 2011 года подбираем и поставляем оборудование для систем отопления, водоснабжения и тепловых пунктов
                    </span> <span class="is-hidden-desktop" data-v-04451c94>
                        Проектируем<br data-v-04451c94>и поставляем системы<br data-v-04451c94>отопления<br data-v-04451c94>и водоснабжения<br data-v-04451c94>с 2011 года
                    </span></h2>
                <p class="has-text-light is-hidden-touch pt-6">
                    Сделаем это и для вас. Закажите расчет сметы!
                </p>
                <p class="has-text-light is-hidden-desktop pt-3-touch" data-v-04451c94>
                    Сделаем это и для вас.<br data-v-04451c94>Закажите расчет сметы!
                </p> <a href="#feedback" class="base-link pt-4 pt-2-touch link icon-arrowRight" data-v-04451c94><span class="base-link__text">Заказать расчет</span>
                    <!----></a>
            </div>
        </div>
        <div class="column is-5 align" data-v-04451c94="">
            <div id="widget-main_catalog_card"></div>
        </div>
    </div>
    <div class="landing-screen is-hidden">
        <div class="columns is-gapless is-justify-content-space-between">
            <div class="column">
                <h2 class="has-text-black">От коттеджей и ЖК<br>
                    до промышленных предприятий</h2>
            </div>
            <div class="column is-flex-shrink-1 is-flex-grow-0 is-hidden-touch"><a href="/cases/" class="base-link h2-right-notice no-wrap link icon-arrowRight"><span class="base-link__text">
                        Посмотреть все кейсы
                    </span>
                    <!----></a></div>
        </div>
        <div class="customers">
            <div class="customers-row">
                <div class="column leroymerlin"><img src="/img/customers/leroymerlin.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column ilim"><img src="/img/customers/ilim.png"> <a href="/" /cases/lass="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column stroycomplex is-hidden-mobile"><img src="/img/customers/stroycomplex.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column newcity is-hidden-mobile"><img src="/upload/medialibrary/86f/86fedb1f870208341f0294166ffd2ec2.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
            </div>
            <hr>
            <div class="customers-row">
                <div class="column rodny_berega is-hidden-mobile"><img src="/img/customers/rodny_berega.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column yanta is-hidden-mobile"><img src="/img/customers/yanta.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column slata is-hidden-mobile"><img src="/img/customers/slata.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column uks is-hidden-mobile"><img src="/img/customers/uks.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column stroycomplex is-hidden-desktop"><img src="/img/customers/stroycomplex.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
                <div class="column newcity is-hidden-desktop"><img src="/img/customers/newcity.png"> <a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Посмотреть кейс</span>
                        <!----></a></div>
            </div>
        </div>
        <a href="/" class="base-link h2-right-notice is-hidden-desktop link icon-arrowRight">
            <span class="base-link__text">
                Посмотреть все кейсы
            </span>
        </a>
    </div>
    <div class="screen-garants">
        <div class="landing-screen">
            <h2 class="has-text-white">
                Гарантии качества
            </h2>
            <div class="screen-garants__content has-text-white">
                <h3>
                    Более 10 лет поставляем оборудование безупречного качества для систем отопления и водоснабжения.
                </h3>
                <div class="pt-4 pt-3-touch">
                    <div id="widget-distributors-block" data-before-text="Цельсий — официальный дистрибьютор" data-after-text="">
                        <img src="/upload/medialibrary/d95/d9554d47d5a6b5a050555c0036beeb42.png" data-name="danfos_prom" data-description="Сертификат дистрибьютора Danfoss" data-title="Danfoss (пром)" data-bx-orig-src="/upload/medialibrary/d95/d9554d47d5a6b5a050555c0036beeb42.png">
                        <img src="/upload/medialibrary/b22/b2221b06d380d978a6de49516063806a.png" data-name="danfos" data-description="Сертификат дистрибьютора Danfoss" data-title="Danfoss" data-bx-orig-src="/upload/medialibrary/b22/b2221b06d380d978a6de49516063806a.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/grundfos.png" data-name="grundfos" data-description="Сертификат дистрибьютора Grundfos" data-title="Grundfos" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/grundfos.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/ridan.png" data-name="ridan" data-description="Сертификат дистрибьютора РИДАН" data-title="РИДАН" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/ridan.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/KAN-Therm.png" data-name="KAN-Therm" data-description="Сертификат дистрибьютора KAN-Therm" data-title="KAN-Therm" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/KAN-Therm.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/Funke.png" data-name="Funke" data-description="Сертификат дистрибьютора Funke" data-title="Funke" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/Funke.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/Volcano.png" data-name="Volcano" data-description="Сертификат дистрибьютора Volcano" data-title="Volcano" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/Volcano.png">
                        <img src="/upload/medialibrary/f33/f33147573bfc7b988a2ab292843295ca.png" data-name="LD" data-description="Сертификат дистрибьютора LD" data-title="LD" data-bx-orig-src="/upload/medialibrary/f33/f33147573bfc7b988a2ab292843295ca.png">
                        <img src="/_nuxt/src/static/img/landing/distributors_certs/KORF.png" data-name="KORF" data-description="Сертификат дистрибьютора KORF" data-title="KORF" data-bx-orig-src="/_nuxt/src/static/img/landing/distributors_certs/KORF.png">
                        <img src="/upload/medialibrary/4a6/4a60f601b7ade0e58d9965eaa146b0de.png" data-name="ADL" data-description="Свидетельство о статусе официального дилера" data-title="ADL" data-bx-orig-src="/upload/medialibrary/4a6/4a60f601b7ade0e58d9965eaa146b0de.png">
                    </div>
                </div>
                <div class="pt-4 pt-3-touch">
                    Собственное сборочное производство и сервисный центр теплообменного оборудования Funke и Ридан.
                </div>
                <div class="pt-4 pt-3-touch">
                    <a href="#feedback" class="base-link is-size-3 is-hidden-touch link-white icon-arrowRight">
                        <span class="base-link__text">
                            Подобрать техническое решение
                        </span>
                        </a>
                        <a href="#feedback" class="base-link is-size-4-touch is-hidden-desktop link-white icon-arrowRight">
                            <span class="base-link__text">
                                Подобрать техническое<br>решение
                            </span>
                        </a>
                </div>
            </div>
        </div>
    </div>
    <div class="landing-screen screen-stats">
        <h2 class="has-text-black">Строго выдерживаем сроки
            поставки,<br>прописанные в договоре.</h2>
        <div class="is-hidden-touch">
            <div class="columns landing-stats">
                <div class="column is-half">
                    <div class="columns is-gapless left-top-border">
                        <div class="column is-flex-shrink-1 is-flex-grow-0">
                            <div class="is-size-2 has-text-black">2000+</div>
                            <div>клиентов</div>
                        </div>
                        <div class="column has-text-centered">
                            <div class="landing-stats__icon"><img src="/img/icons/arrow-right-long.svg"></div>
                        </div>
                        <div class="column">
                            <div class="is-size-2 has-text-black">300+</div>
                            <div>школ<br>и детских садов</div>
                        </div>
                        <div class="column">
                            <div class="is-size-2 has-text-black">50+</div>
                            <div>ЖК</div>
                        </div>
                        <div class="column">
                            <div class="is-size-2 has-text-black">70+</div>
                            <div>объектов<br>энергетики</div>
                        </div>
                    </div>
                </div>
                <div class="column is-half">
                    <div class="columns landing-stats__subrow">
                        <div class="column">
                            <div class="left-top-border">
                                <div class="is-size-2 has-text-black">1000 м<sup class="is-size-5">2</sup></div>
                                <div>складской комплекс</div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="left-top-border">
                                <div class="is-size-2 has-text-black">10 000+</div>
                                <div>товаров</div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="left-top-border">
                                <div class="is-size-2 has-text-black">30+</div>
                                <div>производителей</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-gapless pt-6">
                <div class="column is-half"><a href="/cases/" class="base-link link icon-arrowRight"><span class="base-link__text">Все объекты</span>
                        <!----></a></div>
                <div class="column is-half">
                    <div class="pl-3"><a href="/equipment/" class="base-link link icon-arrowRight"><span class="base-link__text">Поставщики</span>
                            <!----></a></div>
                </div>
            </div>
        </div>
        <div class="landing-stats-mobile is-hidden-desktop">
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">2000+</div>
                    <div>клиентов</div>
                </div>
            </div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">300+</div>
                    <div>школ<br>и детских садов</div>
                </div>
            </div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">50+</div>
                    <div>ЖК</div>
                </div>
            </div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">70+</div>
                    <div>объектов<br>энергетики</div>
                </div>
            </div>
            <div class="column is-full-touch landing-stats-mobile__objects-link"><a href="/" class="base-link link icon-arrowRight"><span class="base-link__text">Все объекты</span>
                    <!----></a></div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">1000 м<sup class="is-size-5">2</sup></div>
                    <div>складской комплекс</div>
                </div>
            </div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">10 000+</div>
                    <div>тысяч товаров</div>
                </div>
            </div>
            <div class="column is-half-touch">
                <div class="left-top-border">
                    <div class="is-size-0-touch has-text-black">16</div>
                    <div>производителей</div>
                </div>
            </div>
            <div class="column is-full-touch landing-stats-mobile__providers-link"><a href="/" class="base-link link icon-arrowRight"><span class="base-link__text">Поставщики</span>
                    <!----></a></div>
        </div>
    </div>
    <div id="widget-landing-screen-price">
        <div id="option-1-title">
            Снизим стоимость сметы или проекта<br>
            без потери качества
        </div>
        <div id="option-1-content">
            <ul class="ul-white">
                <li>Наши опытные сотрудники помогут снизить стоимость сметы и пересогласовать проект, а значит заработать больше, без потери качества и работоспособности технического решения</li>
                <li>Предоставим скидку от производителя</li>
            </ul>
        </div>
        <div id="option-2-title">
            Любой вариант оплаты
        </div>
        <div id="option-2-content">
            <ul class="ul-white">
                <li>Частичная и полная отсрочка оплаты за товар</li>
                <li>Бартерные сделки с недвижимостью</li>
                <li>Работа по банковской гарантии</li>
                <li>Работа с залогом</li>
            </ul>
        </div>
    </div>
</div>
<?
$APPLICATION->IncludeFile(
    SITE_DIR."include/feedback_form.php",
    Array(
        "title" => "Обращайтесь!",
        "text" => "Получите экспертное мнение, подтвержденное 10-летним опытом и, как результат, наиболее выгодное и технически верное решение",
        "name" => "Форма обратной связи"
    ),
    Array("MODE"=>"html")
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>