<?php

if (!isset($title)) $title = "Обращайтесь!";
if (!isset($text)) $text = "Получите экспертное мнение, подтвержденное 10-летним опытом и, как результат, наиболее выгодное и технически верное решение.";
if (!isset($name)) $name = "Форма обратной связи";
if (!isset($has_smeta_block)) $has_smeta_block = true;
if (!isset($zero_x_paddings)) $zero_x_paddings = false;
if (!isset($smeta_email)) $smeta_email = "info@celsiy.pro";
if (!isset($contacts_content)) $contacts_content = NULL;

?>






<div>
    <div class="screen-feedback <?= ($zero_x_paddings ? 'px-0' : '') ?>" id="feedback">
        <div class="screen-feedback__cont">
            <div class="column-3-4 <? ($has_smeta_block ? 'with-divider' : '') ?>">
                <h2 class="has-text-black"><?= $title ?></h2>
                <div class="screen-feedback__text pt-5">
                    <?= $text ?>
                </div>
                <div class="widget-feedback-form" name="<?= $name ?>"></div>
            </div>
            <? if ($has_smeta_block): ?>
                <div class="column-1-4 screen-feedback__smeta">
                    <? if ($contacts_content == NULL): ?>
                        <h3 class="has-text-black">
                            Уже есть готовая смета или проект?
                        </h3>
                        <p class="pt-4">
                            Отправьте её&nbsp;нам
                            <a href="mailto:sales@celsiy.pro"
                               class="base-link pt-4 pt-2-touch link icon-null">
                                <span class="base-link__text">sales@celsiy.pro</span>
                            </a>
                            или свяжитесь с&nbsp;нами по&nbsp;телефону
                            <a href="tel:<?= $GLOBALS['site_phone'] ?>"
                               class="has-text-black"><?= $GLOBALS['site_phone'] ?></a>
                            и&nbsp;мы&nbsp;бесплатно улучшим её&nbsp;без потерь в&nbsp;качестве и&nbsp;с&nbsp;большей
                            выгодой для&nbsp;вас
                        </p>
                    <? else: ?>
                        <?= $contacts_content ?>
                    <? endif; ?>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>