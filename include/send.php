<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/include/recaptcha.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/vendor/google/recaptcha/src/autoload.php");
CModule::IncludeModule("form");
$data = json_decode($_POST["param"]);
$error = '';
$recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_secret);

$resp = $recaptcha->setExpectedAction("submit")
    ->setScoreThreshold(0.5)
    ->verify($data -> recaptcha_token, $_SERVER['REMOTE_ADDR']);
if ($resp->isSuccess()) {
    // Verified!
} else {
    $error .= $resp->getErrorCodes();
}

if ($error) {
    echo "<span style='color:#ff0000;'>" . $error . "</span>";
    http_response_code(400);
} else {
    $arEventFields = array(
        "NAME" => $data->name,
        "CONTACT" => $data->contact,
        "FORM_NAME" => $data->form_name,
        "PAGE" => $data->page,
    );

    $FORM_ID = 1;
    $arValues = array(
        "form_text_1" => $data->name,
        "form_text_2" => $data->contact,
        "form_text_3" => $data->form_name,
        "form_text_4" => $data->page,
    );

    // создадим новый результат
    if ($RESULT_ID = CFormResult::Add($FORM_ID, $arValues)) {
//            echo "Результат #".$RESULT_ID." успешно создан";
        // создадим почтовое событие для отсылки по EMail данных результата
        if (CFormResult::Mail($RESULT_ID))
        {
            echo "Почтовое событие успешно создано.";
        }
        else // ошибка
        {
            global $strError;
            echo $strError;
        }
        http_response_code(200);
    } else {
        global $strError;
        echo $strError;
        http_response_code(500);
    }
}
?>