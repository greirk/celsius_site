<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/local/templates/celsius/head.php");

?>

    <div id="__layout">
        <div>
            <div class="page-404">
                <div class="page-404__logo">
                    <a href="/"><img src="/img/logo.svg" class="logo"></a>
                </div>
                <div class="page-404__content">
                    <img src="/img/404-full.svg" class="is-hidden-touch">
                    <img src="/img/404.svg"
                         class="is-hidden-desktop"
                    >
                    <p class="is-size-3">Страница не найдена</p>
                    <a href="/"
                       class="base-link link icon-arrowRight"
                    >
                        <span class="base-link__text">На главную страницу</span>
                    </a>
                </div>
                <div class="page-404__footer">
                    <span>© 2011–2021, ООО Цельсий, Иркутск</span>
                    <a href="/terms" class="base-link link-main icon-null">
                        <span class="base-link__text">Политика конфиденциальности</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>
<?php